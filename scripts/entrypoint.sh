#!/bin/sh

set -e

python manage.py collectstatic --noinput
python manage.py wait_for_db
python manage.py migrate  ## run defined migrations 

uwsgi --socket :9000 --workers 4 --master --enable-threads --module app.wsgi  ## run as a tcp on port 9000 and map the request from the proxy to this server as a master for Docker purposes (shutdown) 
