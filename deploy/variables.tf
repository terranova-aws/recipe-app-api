variable "prefix" {
  default = "raad"
}

variable "project" {
  default = "recipe-app-api-devops"
}

variable "contact" {
  default = "gaston@isolver.eu"
}

variable "db_username" {
  description = "Username for the RDS Postgres instance"
}

variable "db_password" {
  description = "Password for the RDS postgres instance"
}

variable "bastion_key_name" {
  default = "recipe-app-api-devops-bastion"
}

variable "ecr_image_api" {
  description = "ECR Image for API"
  #default     = "<APP ECR Image URL>:latest"
  default = "533943029581.dkr.ecr.us-east-1.amazonaws.com/terranova/recipe-app-api-devops:latest"
}

variable "ecr_image_proxy" {
  description = "ECR Image for API"
  #default     = "<App ECR Image for Proxy>:latest"
  default = "533943029581.dkr.ecr.us-east-1.amazonaws.com/terranova/recipe-app-api-proxy"
}

variable "django_secret_key" {
  description = "Secret key for Django app"
}
